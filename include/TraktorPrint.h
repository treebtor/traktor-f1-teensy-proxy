#ifndef TRAKTORPRINT_
#define TRAKTORPRINT_

#include "TraktorController.h"

void print_state_nice(TraktorController traktor);
void print_state_raw(TraktorController traktor);
void print_hexbytes(const void *ptr, uint32_t len);

#endif