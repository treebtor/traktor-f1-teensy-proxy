#include <Arduino.h>
#include "TraktorController.h"

//=============================================================================
// Display state of controller on serial console
//=============================================================================
//  e207   e207   e207   e107         Knobs' values
//
//  f60f   f70f   f60f   f60f         Faders' values
//
//  0    0    0         0/0           Buttons' states, includinf encoder value
//
//  0    0    0    0    0             Button's states
//
//  0      0      0      1            Matrix buttons' states
//  0      0      0      0
//  0      0      0      0
//  0      0      0      0
//
//  0      0      0      0			  Stop buttons states
//=============================================================================

void print_state_nice(TraktorController traktor) {

	// this is a try to clear the serial console before each update
	// got it working only with putty
  	Serial.write(27);
  	Serial.print("[2J");    // clear screen command
  	Serial.write(27);
  	Serial.print("[H");     // cursor to home command
	
	Serial.println("==========================================");
	Serial.printf("  %-6x %-6x %-6x %-6x\n\n",
		traktor.get_knob_value(0),
		traktor.get_knob_value(1),
		traktor.get_knob_value(2),
		traktor.get_knob_value(3)
	);

	Serial.printf("  %-6x %-6x %-6x %-6x\n\n",
		traktor.get_fader_value(0),
		traktor.get_fader_value(1),
		traktor.get_fader_value(2),
		traktor.get_fader_value(3)
	);

	// Serial.printf("  %-4u %-4u %-4u      %-1u/%-6x\n\n",
	// 	traktor.is_button_pressed(BUTTON_SYNC),
	// 	traktor.is_button_pressed(BUTTON_QUANT),
	// 	traktor.is_button_pressed(BUTTON_CAPTURE),
	// 	traktor.is_button_pressed(BUTTON_ENCODER),
	// 	traktor.get_encoder_value()
	// );
	
	// Serial.printf("  %-4u %-4u %-4u %-4u %-4u\n\n",
	// 	traktor.is_button_pressed(BUTTON_SHIFT),
	// 	traktor.is_button_pressed(BUTTON_REVERSE),
	// 	traktor.is_button_pressed(BUTTON_TYPE),
	// 	traktor.is_button_pressed(BUTTON_SIZE),
	// 	traktor.is_button_pressed(BUTTON_BROWSE)
	// );
	
	// //Serial.printf("      %4u %4u %4u %4u\n", 1, 2, 3, 4);
    // for(int i=0; i<4; i++) {
    //     Serial.printf("  %-6u %-6u %-6u %-6u\n", 
	// 		traktor.is_button_pressed(31-4*i),
	// 		traktor.is_button_pressed(30-4*i),
	// 		traktor.is_button_pressed(29-4*i),
	// 		traktor.is_button_pressed(28-4*i)
	// 	);
    // }

	// Serial.println();
	// Serial.printf("  %-6u %-6u %-6u %-6u\n",
	// 	traktor.is_button_pressed(BUTTON_STOP_1),
	// 	traktor.is_button_pressed(BUTTON_STOP_2),
	// 	traktor.is_button_pressed(BUTTON_STOP_3),
	// 	traktor.is_button_pressed(BUTTON_STOP_4)
	// );
}

//=============================================================================
void print_state_raw(TraktorController traktor) {
	

	Serial.println("\nFader values");
	for(int i=0;i<4;i++) {
		Serial.printf("%5u %x\n",i,traktor.get_fader_value(i));
	}

	Serial.println("\nKnob values");
	for(int i=0;i<4;i++) {
		Serial.printf("%5u %x",i,traktor.get_knob_value(i));
	}

	Serial.println("\nEncoder value");
	Serial.println(traktor.get_encoder_value());

	Serial.println("\nButton states");
	for(int i=0; i<32; i++) {
		Serial.printf("%5u %5u\n", i, traktor.is_button_pressed(i));
	}
}