//#define DEBUG
#include <debug.h>
#include <math.h>
#include <Arduino.h>
#include "USBHost_t36.h"
#include <TraktorController.h>
#include "MIDIUSB.h"
#include "TraktorPrint.h"
//#include <i2c_t3.h>
#include <wire.h>
#include <i2cEncoderMiniLib.h>
#include <LEDRingSmall_W.h>
#include "Adafruit_NeoTrellis.h"
#include "SPI.h"
//#include "Adafruit_I2CDevice.h"

uint32_t FreeMem(){ // for Teensy 3.0
    uint32_t stackTop;
    uint32_t heapTop;

    // current position of the stack.
    stackTop = (uint32_t) &stackTop;

    // current position of heap.
    void* hTop = malloc(1);
    heapTop = (uint32_t) hTop;
    free(hTop);

    // The difference is (approximately) the free, available ram.
    return stackTop - heapTop;
}

//================================================================
// Teensy Ojbects
//================================================================
#define NB_ENCODER 16
i2cEncoderMiniLib encoder[NB_ENCODER]={
    i2cEncoderMiniLib(1),
    i2cEncoderMiniLib(2),
    i2cEncoderMiniLib(3),
    i2cEncoderMiniLib(4),
    i2cEncoderMiniLib(5),
    i2cEncoderMiniLib(6),
    i2cEncoderMiniLib(7),
    i2cEncoderMiniLib(8),
    i2cEncoderMiniLib(9),
    i2cEncoderMiniLib(10),
    i2cEncoderMiniLib(11),
    i2cEncoderMiniLib(12),
    i2cEncoderMiniLib(13),
    i2cEncoderMiniLib(14),
    i2cEncoderMiniLib(15),
    i2cEncoderMiniLib(16),

};

  
int32_t econfig = ( i2cEncoderMiniLib::WRAP_DISABLE | i2cEncoderMiniLib::DIRE_LEFT | i2cEncoderMiniLib::IPUP_ENABLE | i2cEncoderMiniLib::RMOD_X2 );
int32_t counter = 63;
int32_t maxvalue = 127;
int32_t minvalue = 0;
int32_t Step = 1;
int32_t Step2 = 2;
int32_t Step3 = 3;
int32_t Step4 = 2;
int ValEnc;
int AddEnc;
#define NB_RING 16
LEDRingSmall LED00(ISSI3746_SJ4 | ISSI3746_SJ8,1);
LEDRingSmall LED01(ISSI3746_SJ4 | ISSI3746_SJ7,1);
LEDRingSmall LED02(ISSI3746_SJ3 | ISSI3746_SJ6,1);
LEDRingSmall LED03(ISSI3746_SJ1 | ISSI3746_SJ6,1);
LEDRingSmall LED04(ISSI3746_SJ2 | ISSI3746_SJ5,1);
LEDRingSmall LED05(ISSI3746_SJ3 | ISSI3746_SJ8,1);
LEDRingSmall LED06(ISSI3746_SJ2 | ISSI3746_SJ8,1);
LEDRingSmall LED07(ISSI3746_SJ1 | ISSI3746_SJ8,1);
LEDRingSmall LED08(ISSI3746_SJ2 | ISSI3746_SJ6,0);
LEDRingSmall LED09(ISSI3746_SJ1 | ISSI3746_SJ6,0);
LEDRingSmall LED10(ISSI3746_SJ4 | ISSI3746_SJ6,0);
LEDRingSmall LED11(ISSI3746_SJ3 | ISSI3746_SJ6,0);
LEDRingSmall LED12(ISSI3746_SJ4 | ISSI3746_SJ5,0);
LEDRingSmall LED13(ISSI3746_SJ4 | ISSI3746_SJ7,0);
LEDRingSmall LED14(ISSI3746_SJ2 | ISSI3746_SJ5,0);
LEDRingSmall LED15(ISSI3746_SJ2 | ISSI3746_SJ7,0);
LEDRingSmall *encoder_led[NB_RING]={&LED00,&LED01,&LED15,&LED10,&LED11,&LED14,&LED03,&LED04,
                                    &LED05,&LED02,&LED12,&LED09,&LED08,&LED13,&LED07,&LED06
                                    };


//================================================================
// USB Host Ojbects
//================================================================
USBHost myusb;
USBHub hub1(myusb);
USBHIDParser hid1(myusb);
USBHIDParser hid2(myusb);
TraktorController f1_1(myusb);
TraktorController f1_2(myusb);
Adafruit_NeoTrellis trellis1(0x2E,&Wire);
Adafruit_NeoTrellis trellis2(0x2E,&Wire1);
USBHIDInput *hiddrivers[] = {
    &f1_1,
    &f1_2
};

#define CNT_HIDDEVICES (sizeof(hiddrivers)/sizeof(hiddrivers[0]))

// Provision made for 2 F1
const char * hid_driver_names[CNT_HIDDEVICES] = {
    "f1_1",
    "f1_2"
};

bool hid_driver_active[CNT_HIDDEVICES] = {
    false,
    false
};
bool init=true;
bool init2 = true;
bool init_tab[2]={init,init2};
bool new_device_detected = false;
bool show_changed_only = false;
midiEventPacket_t rx;
bool init_done=true;
unsigned long timer_falling_edge = 0;
unsigned long timer_rising_edge= 0;
#define UNDEFINE_VAL 255
#define NB_MESSAGE_RECORD 30
#define NB_CONTROLLER 3
#define NB_F1 2
#define DECK_A 0
#define DECK_B 1
#define DECK_C 2
#define DECK_D 3

//line for tab value F1
#define PRE_VAL_F1 0
#define REC_PRE_VAL_F1 1
#define REC2_PRE_VAL_F1 2
#define REC_PRE_VAL_ENCODERS 3

//line for tab value encoders
#define PRE_VAL 0
#define REC_PRE_VAL 1
uint8_t encoder_value[16];
uint8_t receive_encoder_value[16];
control_change encoder_message[NB_ENCODER]; 

// value for encoders
#define MID_ENC_VAL 63
#define MAX_ENC_VAL 127
#define MIN_ENC_VAL 0
#define LED_CENTRE_HAUT 0
#define LED_CENTRE_BAS 12
#define LED_MAX_HAUT_GAUCHE 6
#define LED_MAX_HAUT_DROITE 19
#define LED_MAX_BAS_GAUCHE 7
#define LED_MAX_BAS_DROITE 18
#define MIDI_CENTER_VALUE 64
#define COLOR_MAX_HAUT 0xFF1010
#define COLOR_0_HAUT 0xFF0520
#define COLOR_1_HAUT 0x0033FF           //0xFF3333//peche
#define COLOR_2_HAUT 0xCC3399//violet clair
#define COLOR_0_BAS 0xFF0000 // rouge milieu
#define COLOR_1_BAS 0x003300//?
#define COLOR_2_BAS 0xCC3300//??

#define ORANGE 0xFF8000
#define VIOLET 0x7F00FF
#define BLEU_CLAIR 0x66B2FF
#define BLEU_PALE 0x001515//PAD
#define BLEU_VIOLET 0x5000FF//PAD
#define VERT_CLAIR 0x33FF33
#define VERT_CLAIR2 0x14FF14 //PAD
#define ROSE 0xFF99FF
#define ROSE_CLAIR 0x3C1014//PAD


#define REC 0
#define LAUNCH 1
#define CAPTURE 2
#define FADER 7
#define KNOB 6

uint8_t change_f1_id;
uint8_t	unchange_f1_id;
uint8_t	button_id=255;
uint8_t fader_id=255;
uint8_t knob_id=255;
uint8_t encoder_id=255;
uint8_t traktor_id=255;
bool launch_on_0=false;
bool launch_on_1=false;
bool midi_reveive_enc_value=false;
int rec_bank=255;
//prototypes

void update_pad(uint8_t button_id,uint8_t f1_id);
void midi_send_to_traktor(uint8_t channel,uint8_t cc,uint8_t value);
void set_record_mode(uint8_t f1_id);
void launch_record(uint8_t rec_bank);
void fader_on(uint8_t f1_id);
void knob_on(uint8_t f1_id);
void button_pressed_MIDI(uint8_t button_id, uint8_t f1_id);
void button_pressed_intern(uint8_t button_id, uint8_t f1_id);
void fader_change(uint8_t fader_id, uint8_t f1_id);
void knob_change(uint8_t knob_id, uint8_t f1_id);
void button_pressed_rec(uint8_t button_id, uint8_t f1_id, uint8_t rec_f1_id );
void launch_by_hotcue_on(uint8_t f1_id);
void change_rec_knob_and_fader(uint8_t f1_id);
void store_encoder_message(uint8_t encoder_id);
void led_off_cadrant_gauche(u_int8_t id);
void led_off_cadrant_droit(u_int8_t id);
void led_off_rec_cadrant_gauche(u_int8_t id);
void led_off_rec_cadrant_droit(u_int8_t id);
void RingUpdate(u_int8_t value, u_int8_t id);
void TrellisUpdate(u_int8_t trellis,u_int8_t value, u_int8_t id);
void Rec_RingUpdate(int value, int id, int f1);
void ajustencstep(int value, int i);
void encread();
void F1_plug();
void F1_read();
void set_initial_color(uint8_t button_id, uint8_t f1_id);
void update_f1_button_led(uint8_t f1_id, byte data1,byte data2);
void traktor_set_colors();
void update_digit(uint8_t f1_id, uint8_t value);//non utilisé
void reset_digit(uint8_t f1_id);//non utilisé
void update_device_info();
void set_f1_initial_state(uint8_t f1_id);
void store_in_bank (uint8_t button_id, uint8_t f1_id, uint8_t rec_bank );
void sent_rise_values_and_update_leds(uint8_t f1_id);
void sent_fall_rec_values(uint8_t f1_id);
void sent_encoders_message();
void reset_encoders_and_leds();
void reset_encoder_values_and_leds(int encoderIndex);
void encoder_push(i2cEncoderMiniLib* obj);
void encoder_double_push(i2cEncoderMiniLib* obj);
// void encoder_max_eq(i2cEncoderMiniLib * obj);
// void encoder_max_filter(i2cEncoderMiniLib * obj);
// void encoder_min_eq(i2cEncoderMiniLib * obj);
// void encoder_min_filter(i2cEncoderMiniLib * obj);
TrellisCallback fx_on(keyEvent evt);
TrellisCallback fx_on2(keyEvent evt);

TraktorController *all_f1[2]={&f1_1, &f1_2};
Adafruit_NeoTrellis *trellis[2]={&trellis1,&trellis2};


void midi_send_to_traktor(uint8_t channel,uint8_t cc,uint8_t value)
{
    if(cc!=255)
    {
        Serial.printf("midi sent: channel:%d \t cc:%d \t value: %d \n",channel,cc,value );
	    midiEventPacket_t event = { 0x0B, (uint8_t)(0xB0 | channel), cc, value};
	    MidiUSB.sendMIDI(event);
    }

}
//================================================================
// 
//================================================================

void update_pad(uint8_t button_id,uint8_t f1_id)// eteint le bouton precedent'
{
    if(button_id < 16)
    {
        for(int i=8; i<16; i++)
        {
            if (i==button_id || all_f1[f1_id]->value[REC_PRE_VAL_F1][i]!=255 || all_f1[f1_id]->value[REC2_PRE_VAL_F1][i]!=255  )
            {
            continue;
            }
            else
            {
                all_f1[f1_id]->set_led(i,all_f1[f1_id]->mapping[i].play_color_off);

            }        
        }
    }
    else if (button_id>15 && button_id<25)
    {
        for(int i=16; i<24; i++){
            if (i==button_id || all_f1[f1_id]->value[REC_PRE_VAL_F1][i]!=255|| all_f1[f1_id]->value[REC2_PRE_VAL_F1][i]!=255)
            {
                continue;
            }
            else
            {
                all_f1[f1_id]->set_led(i,all_f1[f1_id]->mapping[i].play_color_off);
            }
                    
        }
    }        
}
//================================================================
// Record mode
//================================================================

// record mode on/off
//
void set_record_mode(uint8_t f1_id)
{
    rec_bank=f1_id;
    if(all_f1[f1_id]->value[PRE_VAL_F1][REC]==127)
    {
	    for(int i = 0; i<2; i++)
        {
            if(i!=f1_id)
            {
                all_f1[i]->set_led(REC,all_f1[i]->mapping[REC].play_color_off);
                all_f1[i]->value[PRE_VAL_F1][REC]=0;                
            }
        }
        for(int j = 0; j<NB_ENCODER; j++)
        {
            encoder_value[j]=encoder[j].readCounterByte();
        }
    }
    else
    {
	    rec_bank=UNDEFINE_VAL;
        for(int k = 0; k<NB_ENCODER; k++)
        {
            encoder[k].writeCounter((int32_t) encoder_value[k]);
        }
    }
     for(int l = 0; l<2; l++)
     {
        all_f1[l]->send_to_f1();
     }
}

// launch record message
//
void sent_rise_values_and_update_leds(uint8_t f1_id) 
{
    for (int j = 0; j < 2 ; j++)
    {
        for (int i = 0; i < TRAKTOR_F1_BUTTON_COUNT; i++) 
        {

            if (all_f1[j]->value[f1_id+1][i] < 254)
            {
                midi_send_to_traktor(all_f1[j]->channel, all_f1[j]->value[f1_id+1][i], 127);
                update_pad(i, j);
                all_f1[j]->set_led(i, all_f1[j]->mapping[i].play_color_on);
                
            }
        }
        all_f1[j]->send_to_f1();
    }
}

void sent_fall_rec_values(uint8_t f1_id)
{
for (int i = 0; i < TRAKTOR_F1_BUTTON_COUNT; i++) 
    {
        for (int j = 0; j < 2 ; j++)
        {
            if (all_f1[j]->value[f1_id+1][i] < 254)
            {
                midi_send_to_traktor(all_f1[j]->channel, all_f1[j]->value[f1_id+1][i], 0);
	            all_f1[j]->value[f1_id+1][i] = 255;
            }

        }
        
    }
}

void sent_encoders_message(){
	for (int i = 0; i < NB_ENCODER; i++)
    	{
       		if (encoder_message[i].channel!=255)
       		{
           		midi_send_to_traktor(3,encoder_message[i].control,encoder_message[i].value);
       		}

	}
}

void encoder_push(i2cEncoderMiniLib* obj){
    obj->writeCounter(0);
    midi_send_to_traktor(3, obj->id, 0);
    encoder_value[obj->id] =0;
    RingUpdate(0, obj->id);
}
void encoder_double_push(i2cEncoderMiniLib* obj){
    obj->writeCounter(63);
    midi_send_to_traktor(3, obj->id, 63);
    encoder_value[obj->id] =63;
    RingUpdate(63, obj->id);
}
void reset_encoders_and_leds() {
    for (int i = 0; i < NB_ENCODER; i++) {
        reset_encoder_values_and_leds(i);
    }
}

void reset_encoder_values_and_leds(int encoderIndex) {
    encoder_message[encoderIndex].channel = 255;
    encoder_message[encoderIndex].control = 0;
    encoder_message[encoderIndex].value = 0;

    for (int j = LED_MAX_BAS_GAUCHE; j < LED_MAX_BAS_DROITE+1; j++) {
        encoder_led[encoderIndex]->LEDRingSmall_Set_RGB(j, 0x0);
    }
}
void launch_record(uint8_t f1_id)
{
    all_f1[f1_id]->set_led(LAUNCH,all_f1[f1_id]->mapping[LAUNCH].play_color_off);
    all_f1[f1_id]->set_led(REC,all_f1[f1_id]->mapping[REC].play_color_off);
    rec_bank=UNDEFINE_VAL;

    for (int j = 0; j < 2 ; j++)
    {
	    all_f1[j]->value[PRE_VAL_F1][REC]=0;
        all_f1[j]->send_to_f1();
    }
    
    sent_rise_values_and_update_leds(f1_id);
    sent_encoders_message(); //pas de distinction entre le F1
    reset_encoders_and_leds();
    change_rec_knob_and_fader(f1_id);
    sent_fall_rec_values(f1_id);
    
}


void fader_on(uint8_t f1_id)
{
    for (int i = 0; i < 4; i++)
    {
        midi_send_to_traktor(all_f1[f1_id]->channel,i+30, all_f1[f1_id]->value_fader[i]);
    }
    if(all_f1[f1_id]->value[PRE_VAL_F1][FADER]==127)
    {
        for (int j = 0; j < 2; j++)
        {
            if(f1_id==j)
            {
                all_f1[j]->fader_on=true;
                all_f1[j]->set_led(FADER,all_f1[j]->mapping[FADER].play_color_on);
            }
            else
            {
                all_f1[j]->fader_on=false;
                all_f1[j]->value[PRE_VAL_F1][FADER]=0;
                all_f1[j]->set_led(FADER,all_f1[j]->mapping[FADER].play_color_off);
            }
        }
    }
    else
    {
        all_f1[f1_id]->fader_on=false;
        all_f1[f1_id]->set_led(FADER,all_f1[f1_id]->mapping[FADER].play_color_off);
    }
    for (int k = 0; k < 2; k++)
    {
        all_f1[k]->send_to_f1();
    }    
}

void knob_on(uint8_t f1_id)
{
    for (int i = 0; i < 4; i++)
    {
        midi_send_to_traktor(all_f1[f1_id]->channel,i+34, all_f1[f1_id]->value_knob[i]);
    }
    if(all_f1[f1_id]->value[PRE_VAL_F1][KNOB]==127)
    {
        for (int j = 0; j < 2; j++)
        {
            if(f1_id==j)
            {
                all_f1[j]->knob_on=true;
                all_f1[j]->set_led(KNOB,all_f1[j]->mapping[KNOB].play_color_on);
            }
            else
            {
                all_f1[j]->knob_on=false;
                all_f1[j]->value[PRE_VAL_F1][KNOB]=0;
                all_f1[j]->set_led(KNOB,all_f1[j]->mapping[KNOB].play_color_off);
            }
        }
    }
    else
    {
        all_f1[f1_id]->knob_on=false;
        all_f1[f1_id]->set_led(KNOB,all_f1[f1_id]->mapping[KNOB].play_color_off);
    }
    for (int k = 0; k < 2; k++)
    {
        all_f1[k]->send_to_f1();
    }
} 

void button_pressed_MIDI(uint8_t button_id, uint8_t f1_id)
{
    if ((bitRead(all_f1[f1_id]->in_state.rising_edge,all_f1[f1_id]->get_bit_position_with_id [button_id]))==1)
    {
        midi_send_to_traktor(all_f1[f1_id]->channel,button_id,127);
        all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].play_color_on);
        update_pad(button_id,f1_id);
    }
    else if ((bitRead(all_f1[f1_id]->in_state.falling_edge,all_f1[f1_id]->get_bit_position_with_id [button_id]))==1)
    {
        midi_send_to_traktor(all_f1[f1_id]->channel,button_id,0);   
    } 

    all_f1[f1_id]->send_to_f1();
}

void button_pressed_intern(uint8_t button_id, uint8_t f1_id)
{
    if ((bitRead(all_f1[f1_id]->in_state.rising_edge,all_f1[f1_id]->get_bit_position_with_id [button_id]))==1)
    {
        if (all_f1[f1_id]->value[PRE_VAL_F1][button_id]==0)
        {
            all_f1[f1_id]->value[PRE_VAL_F1][button_id]=127;
            all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].play_color_on);
        }
        else
        {
               all_f1[f1_id]->value[PRE_VAL_F1][button_id]=0; 
               all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].play_color_off);
        }     
    }
    switch (button_id)
    {
    case REC:
        set_record_mode(f1_id);
        break;
    case LAUNCH:
        launch_record(f1_id);
        break;
    case FADER:
        fader_on(f1_id);
        break;
    case KNOB:
        knob_on(f1_id);
        break;
    case CAPTURE:
        launch_by_hotcue_on(f1_id);
        break;           
    default:
        break;
    }
    all_f1[f1_id]->send_to_f1();
}
void fader_change(uint8_t fader_id, uint8_t f1_id)
{
    if (all_f1[f1_id]->fader_on==true)
    {
       midi_send_to_traktor(all_f1[f1_id]->channel,fader_id, all_f1[f1_id]->value_fader[fader_id-30]);
    } 
}
void knob_change(uint8_t knob_id, uint8_t f1_id)
{
    if (all_f1[f1_id]->knob_on==true)
    {
        midi_send_to_traktor(all_f1[f1_id]->channel,knob_id, all_f1[f1_id]->value_fader[knob_id-30]);
    } 
}

void change_rec_knob_and_fader(uint8_t f1_id)
{
for (int i = 6; i < 8; i++) 
    {
        for (int j = 0; j < 2 ; j++)
        {
            if (all_f1[j]->value[f1_id+1][i] < 254)
            {
		        if(i==6){
                    knob_on(j);
                }
                else
                {
                    fader_on(j);
                }    
            }

        }
        
    }
}
void button_pressed_rec(uint8_t button_id, uint8_t f1_id, uint8_t rec_bank )
{
   all_f1[rec_bank]->set_led(LAUNCH,all_f1[rec_bank]->mapping[LAUNCH].play_color_on);
   store_in_bank ( button_id,f1_id,rec_bank);
}

void launch_by_hotcue_on(uint8_t f1_id)
{
    if(all_f1[f1_id]->value[PRE_VAL_F1][CAPTURE]==127){
    		for(int i = 0; i<2; i++)
        	{
            		if(i!=f1_id)
            		{
                		all_f1[i]->set_led(CAPTURE,all_f1[i]->mapping[CAPTURE].play_color_off);
                		all_f1[i]->value[PRE_VAL_F1][CAPTURE]=0;                
            		}
                all_f1[i]->send_to_f1();    
        	}
	}
    
}

void store_in_bank (uint8_t button_id, uint8_t f1_id, uint8_t rec_bank )
{
    if ((bitRead(all_f1[f1_id]->in_state.rising_edge,all_f1[f1_id]->get_bit_position_with_id [button_id]))==1)
    {
        if(all_f1[f1_id]->value[rec_bank+1][button_id]==button_id)
        {
            all_f1[f1_id]->value[rec_bank+1][button_id]=255;
            all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].play_color_off);
        }
    else
        {
            all_f1[f1_id]->value[rec_bank+1][button_id]=button_id;
            all_f1[f1_id]->set_led(button_id,all_f1[rec_bank]->mapping[button_id].record_color_on);
        }
            all_f1[f1_id]->send_to_f1();
    }
	if(button_id<8){

		if (all_f1[f1_id]->value[PRE_VAL_F1][button_id]==0)
        {
            all_f1[f1_id]->value[PRE_VAL_F1][button_id]=127;
            all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].record_color_on);
        }
        else
        {
               all_f1[f1_id]->value[PRE_VAL_F1][button_id]=0; 
               all_f1[f1_id]->set_led(button_id,all_f1[f1_id]->mapping[button_id].play_color_off);
        }

	}    
}

//================================================================
// MIDI to F1
//================================================================
/*  {127,127,0} //fuschia
    {0,127,127}  //jaune
    { {127,0,0} //bleu
    { {0,127,0} //rouge
    { {0,0,127}  //vert
    { {127,0,127}//turquoise
    { {127,127,127}//blanc
    { {63,0,127} //vert claire
    { {127,63,0} //violet
    { {127,0,63}//bleu ciel
    { {63,127,63}//chaire
    { {30,100,127} //vert d'eau
      {30,127,100} //orange claire   
    { {127,127,63} //rose
    { {63,127,127}//jaune pale
    { {90,100,127} //mojito */
midi_to_f1_mapping mapping2[TRAKTOR_F1_BUTTON_COUNT] = {
//   color_off     color_on     r_color_off  r_color_on       cc_off        cc_on

    { {40,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 0, 0},  {1, 0, 127} },  //SYNC       0
    { {15,0,0},   {127,0,0},    {127,0,0},   {127,0,0},     {1, 1, 0},  {1, 1, 127} },  //QUANT      1
    { {15,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 2, 0},  {1, 2, 127} },  //CAPTURE    2
    { {15,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 3, 0},  {1, 3, 127} },  //SHIFT      3
    { {15,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 4, 0},  {1, 4, 127} },  //REVERSE    4
    { {15,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 5, 0},  {1, 5, 127} },  //TYPE       5
    { {15,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 6, 0},  {1, 6, 127} },  //SIZE       6
    { {0,0,0},   {127,0,0},    {64,0,0},   {100,0,0},     {1, 7, 0},  {1, 7, 127} },  //BROWSE     7
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 8, 0} , {1, 8, 127 } },  //MATRIX_01  8
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 9, 0},  {1, 9,127 } },  //MATRIX_02  9
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 10, 0}, {1, 10, 127} }, //MATRIX_03 10
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 11, 0}, {1, 11, 127} }, //MATRIX_04 11
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 12, 0}, {1, 12, 127} }, //MATRIX_05 12
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 13, 0}, {1, 13, 127} }, //MATRIX_06 13
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 14, 0}, {1, 14, 127} }, //MATRIX_07 14
    { {5,10,40}, {80,80,127},  {5,80,5}, {10,127,10},   {1, 15, 0}, {1, 15, 127} }, //MATRIX_08 15
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 16, 0}, {1, 16, 127} }, //MATRIX_09 16
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 17, 0}, {1, 17, 127} }, //MATRIX_10 17
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 18, 0}, {1, 18, 127} }, //MATRIX_11 18
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 19, 0}, {1, 19, 127} }, //MATRIX_12 19
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 20, 0}, {1, 20, 127} }, //MATRIX_13 20
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 21, 0}, {1, 21, 127} }, //MATRIX_14 21
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 22, 0}, {1, 22, 127} }, //MATRIX_15 22
    { {10,30,15},  {100,127,100}, {5,80,5}, {10,127,10}, {1, 23, 0}, {1, 23, 127} }, //MATRIX_16 23
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 24, 0}, {1, 24, 127} }, //STOP_1    24
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 25, 0}, {1, 25, 127} }, //STOP_2    25
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 26, 0}, {1, 26, 127} }, //STOP_3    26
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 27, 0}, {1, 27, 127} }, //STOP_4    27
};
midi_to_f1_mapping mapping[TRAKTOR_F1_BUTTON_COUNT] = {
    { {64,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 0, 0},  {1, 0, 127} },  //SYNC       0
    { {10,0,0},   {127,0,0},    {127,0,0},   {127,0,0},     {1, 1, 0},  {1, 1, 127} },  //QUANT      1
    { {10,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 2, 0},  {1, 2, 127} },  //CAPTURE    2
    { {10,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 3, 0},  {1, 3, 127} },  //SHIFT      3
    { {10,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 4, 0},  {1, 4, 127} },  //REVERSE    4
    { {10,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 5, 0},  {1, 5, 127} },  //TYPE       5
    { {10,0,0},   {127,0,0},    {64,0,0},   {127,0,0},     {1, 6, 0},  {1, 6, 127} },  //SIZE       6
    { {0,0,0},   {127,0,0},    {64,0,0},   {100,0,0},     {1, 7, 0},  {1, 7, 127} },  //BROWSE     7
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 8, 0},  {2, 8, 127} },  //MATRIX_01  8
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 9, 0},  {2, 9, 127} },  //MATRIX_02  9
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 10, 0}, {2, 10, 127} }, //MATRIX_03 10
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 11, 0}, {2, 11, 127} }, //MATRIX_04 11
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 12, 0}, {2, 12, 127} }, //MATRIX_05 12
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 13, 0}, {2, 13, 127} }, //MATRIX_06 13
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 14, 0}, {2, 14, 127} }, //MATRIX_07 14
    { {40,10,10}, {127,110,110}, {70,70,0}, {110,110,0}, {2, 15, 0}, {2, 15, 127} }, //MATRIX_08 15
    { {30,5,30}, {110,80,120}, {70,70,0}, {110,110,0},   {2, 16, 0}, {2, 16, 127} }, //MATRIX_09 16
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 17, 0}, {2, 17, 127} }, //MATRIX_10 17
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 18, 0}, {2, 18, 127} }, //MATRIX_11 18
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 19, 0}, {2, 19, 127} }, //MATRIX_12 19
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 20, 0}, {2, 20, 127} }, //MATRIX_13 20
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 21, 0}, {2, 21, 127} }, //MATRIX_14 21
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 22, 0}, {2, 22, 127} }, //MATRIX_15 22
    { {30,5,30}, {110,80,110}, {70,70,0}, {110,110,0},   {2, 23, 0}, {2, 23, 127} }, //MATRIX_16 23
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 24, 0}, {1, 24, 127} }, //STOP_1    24
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 25, 0}, {1, 25, 127} }, //STOP_2    25
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 26, 0}, {1, 26, 127} }, //STOP_3    26
    { {0,0,0},   {127,127,0},  {63,0,0},   {127,0,0},   {1, 27, 0}, {1, 27, 127} }, //STOP_4    27
};

//send MIDI message to traktor
//


//=============================================================================
// Encoder an led 
//=============================================================================
void store_encoder_message(uint8_t encoder_id){
    control_change message;
    message.channel= 4;
    message.control=encoder_id;
   if(rec_bank==0){
        message.value=all_f1[0]->value[REC_PRE_VAL_ENCODERS][encoder_id];
    }else{
        message.value=all_f1[1]->value[REC_PRE_VAL_ENCODERS][encoder_id];
    }
    encoder_message[encoder_id]=message;
}

void led_off_cadrant_gauche(u_int8_t id){
    for (int i = LED_CENTRE_HAUT+1; i <= LED_MAX_HAUT_GAUCHE; i++)
    {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x0);
    }
}
void led_off_cadrant_droit(u_int8_t id){
    for (int i = LED_MAX_HAUT_DROITE; i < 24; i++)
    {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x0);
    }
}
void led_off_rec_cadrant_gauche(u_int8_t id){
    for (int i = LED_MAX_BAS_GAUCHE; i <= (LED_CENTRE_BAS-1); i++)
    {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x0);
    }
}
void led_off_rec_cadrant_droit(u_int8_t id){
    for (int i = (LED_CENTRE_BAS+1); i < LED_MAX_BAS_DROITE; i++)
    {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x0);
    }
}

void RingUpdate(u_int8_t value, u_int8_t id){
    u_int8_t cadrant_gauche = (6-(value/12));
    u_int8_t cadrant_droit= (29-(value/12));
    //led_off_rec_cadrant_droit(id);
    //led_off_rec_cadrant_gauche(id);
    Serial.printf("value %d id: %d \n",value, id);
    Serial.printf("gauche %d droit: %d \n",cadrant_gauche, cadrant_droit);
    uint32_t color_max = 0x00;
    uint32_t color_0 = 0x00;
    uint32_t color_on = 0x00;
    uint32_t color_off = 0x00;
    if(id>3)
    {
        color_max=0xFF0510;
        color_0=0xFF0520;
        color_on=0xff2030;
        color_off = 0x101020;
    }else{
        color_max=0x155070;
    color_0=0xEEFFF50;
        color_on=0xFF9900;
        color_off = 0x102010;
    }
    //encoder_led[id]->LEDRingSmall_Set_RGB(0, 0xFF00CC);
    if (value==0)
    {
        encoder_led[id]->LEDRingSmall_Set_RGB(0, color_0);
        for(int i = 1; i<7; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_max);
            }
        for(int i = 18; i<24; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }
    }
    if (value>0 && value<63)
    {
        for(int i = 1; i<cadrant_gauche; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_on);
            }
            for(int i = 6; i >cadrant_gauche; i--){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }
        for(int i = 18; i<24; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }    
    }
    if (value==63)
    {
        encoder_led[id]->LEDRingSmall_Set_RGB(0, color_0);
        for(int i = 1; i<7; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }
        for(int i = 18; i<24; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }
    }

    if (value>63&&value<127)
    {
        for(int i = 18; i<cadrant_droit; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }
            for(int i = 23; i>cadrant_droit-1; i--){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_on);
            }
        for(int i = 1; i<7; i++){
                encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
            }    
    }
}

void Rec_RingUpdate(int value, int id, int f1)
{
    float led_interval=0;
    uint8_t diviseur =10;
    uint8_t led_number=0;
    uint8_t color_number;
    uint8_t cadrant_gche_full=0;
    uint8_t cadrant_dte_full=0;
    uint32_t color_full=0xFF0000;

    uint32_t color_1=0x751075;
    uint32_t color_2=0x991099;
    uint32_t color_3=0xAB10AB;
    uint32_t color_4=0xCB00CB;
    uint32_t color_5=0xED00ED;
    uint32_t color_6=0xFE00FE;
    uint32_t color_filled[5];
    uint32_t color_0 = 0xFF0520;
    uint32_t color_off = 0x000000;
    uint32_t color_STD = 0x101025;

	if(value<74 &&value>54){
  	  diviseur=10;
      
	}else{
  	  diviseur=13;
    
	}

    led_interval=(abs(value-63)/diviseur);
    led_number=(uint8_t)floor(led_interval);
    color_number=(abs(value-63)%diviseur);
    cadrant_gche_full=12-led_number;
    cadrant_dte_full=12+led_number;
    Serial.printf("value: %d \n color num:%d \n  led num: %d \n" ,value,color_number,led_number);
    switch (color_number)
        {
        case 0:
            color_filled[0]=color_0;
	    color_filled[1]=color_STD;
 	    color_filled[2]=color_STD; 
	    color_filled[3]=color_STD;
	    color_filled[4]=color_STD;    
            break;
        case 1:
            color_filled[0]=color_0;
	    color_filled[1]=color_1;
 	    color_filled[2]=color_STD; 
	    color_filled[3]=color_STD;
	    color_filled[4]=color_STD; 
            break;
        case 2:
            color_filled[0]=color_0;
	    color_filled[1]=color_1;
 	    color_filled[2]=color_1; 
	    color_filled[3]=color_STD;
	    color_filled[4]=color_STD; 
            break;
        case 3:
            color_filled[0]=color_0;
	    color_filled[1]=color_2;
 	    color_filled[2]=color_1; 
	    color_filled[3]=color_1;
	    color_filled[4]=color_STD; 
            break;
        case 4:
            color_filled[0]=color_0;
	    color_filled[1]=color_2;
 	    color_filled[2]=color_2; 
	    color_filled[3]=color_1;
	    color_filled[4]=color_1;
            break; 
        case 5:
            color_filled[0]=color_0;
	    color_filled[1]=color_3;
 	    color_filled[2]=color_2; 
	    color_filled[3]=color_2;
	    color_filled[4]=color_1;
            break; 
        case 6:
            color_filled[0]=color_0;
	    color_filled[1]=color_3;
 	    color_filled[2]=color_3; 
	    color_filled[3]=color_2;
	    color_filled[4]=color_2;
            break; 
        case 7:
            color_filled[0]=color_0;
	    color_filled[1]=color_4;
 	    color_filled[2]=color_3; 
	    color_filled[3]=color_3;
	    color_filled[4]=color_2;
            break; 
        case 8:
            color_filled[0]=color_0;
	    color_filled[1]=color_4;
 	    color_filled[2]=color_4; 
	    color_filled[3]=color_3;
	    color_filled[4]=color_3;
            break;  
        case 9:
            color_filled[0]=color_0;
	    color_filled[1]=color_5;
 	    color_filled[2]=color_4; 
	    color_filled[3]=color_4;
	    color_filled[4]=color_3;
            break;  
        case 10:
            color_filled[0]=color_0;
	    color_filled[1]=color_5;
 	    color_filled[2]=color_5; 
	    color_filled[3]=color_4;
	    color_filled[4]=color_4;
            break; 
        case 11:
            color_filled[0]=color_0;
	    color_filled[1]=color_6;
 	    color_filled[2]=color_5; 
	    color_filled[3]=color_5;
	    color_filled[4]=color_4;
            break;
        case 12:
            color_filled[0]=color_0;
	    color_filled[1]=color_6;
 	    color_filled[2]=color_6;
	    color_filled[3]=color_5;
	    color_filled[4]=color_5;
            break; 
        default:
            break;
        }
    
    if (value==0)
    {
        encoder_led[id]->LEDRingSmall_Set_RGB(12, color_full);
        for(int i = 8; i<12; i++){
            encoder_led[id]->LEDRingSmall_Set_RGB(i, color_full);
        }
        for(int i = 13; i<18; i++){
            encoder_led[id]->LEDRingSmall_Set_RGB(i, color_off);
        }
    }
    if (value>0 && value<63)
    {
        for(int i = 12; i>cadrant_gche_full; i--)
        {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, color_0);
        }
        encoder_led[id]->LEDRingSmall_Set_RGB(cadrant_gche_full, color_0);
	
        for(int j=0; j<5 ; j++){
		    if(cadrant_gche_full>7+j)
            {
	            encoder_led[id]->LEDRingSmall_Set_RGB(cadrant_gche_full-j, color_filled[j]);
	        }

            for(int i = 13; i<18; i++){
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x000000);
            }    
        }
    }
    if (value==63)
    {
        encoder_led[id]->LEDRingSmall_Set_RGB(12, color_0);
        for(int i = 8; i<12; i++){
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x00);
        }
        for(int i = 13; i<18; i++){
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x00);
        }
    }
    if (value>63&&value<127)
    {
        for(int i = 12; i<cadrant_dte_full; i++)
        {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, color_0);
        }
        for(int j=0; j<5; j++){
		    if(cadrant_dte_full<18-j)
            {
	            encoder_led[id]->LEDRingSmall_Set_RGB(cadrant_dte_full+j, color_filled[j]);
	        }
            for(int i = 8; i<12; i++)
            {
            encoder_led[id]->LEDRingSmall_Set_RGB(i, 0x00);
            }
        }
    }   
}

void ajustencstep(int value, int i)
{

    if (value > 62 || value < 64 || value > 126)
    {
        encoder[i].writeStep(Step);
    }
    if ((value < 61 && value > 45) || (value > 65 && value < 80))
    {
        encoder[i].writeStep(Step2);
    }
    if ((value < 45 && value > 20) || (value > 80 && value < 92))
    {
        encoder[i].writeStep(Step3);
    }
        if ((value < 20 || value > 92) && value < 126)
    {
        encoder[i].writeStep(Step4);
    }
}
void encread()
{
    //Serial.printf("ENCREAD\n ");
    for (int i = 0; i < NB_ENCODER; i++)

    {
         if (rec_bank==0)
        {
            if (encoder[i].updateStatus())
                {
                    ValEnc = encoder[i].readCounterByte();
                    ajustencstep(encoder[i].readCounterByte(), i);
                    AddEnc = i;
                    all_f1[0]->value[REC_PRE_VAL_ENCODERS][AddEnc] = ValEnc;
                    store_encoder_message(AddEnc);
                    Rec_RingUpdate(ValEnc, AddEnc, 0);
                }
        }
        else if (rec_bank==1)
        {
            if (encoder[i].updateStatus())
            {
                ValEnc = encoder[i].readCounterByte();
                ajustencstep(encoder[i].readCounterByte(), i);
                AddEnc = i;
                all_f1[1]->value[REC_PRE_VAL_ENCODERS][AddEnc] = ValEnc;
                store_encoder_message(AddEnc);
                Rec_RingUpdate(ValEnc, AddEnc, 1);
            }
            }
            else
            {
                if (encoder[i].updateStatus())
                {
                    ajustencstep(encoder[i].readCounterByte(), i);
                    ValEnc = encoder[i].readCounterByte();
                    AddEnc = i;
                    encoder_value[i] = ValEnc;
                    if (midi_reveive_enc_value==false)
                    {
                        
                        midi_send_to_traktor(3, AddEnc, ValEnc);
                    }
                    RingUpdate(ValEnc, i);
                    
                }
            }
                       
        }
        midi_reveive_enc_value=false;
}

void set_initial_color(uint8_t button_id, uint8_t f1_id)
{
    //Serial.printf("f1id_init_color %d \n", all_f1[f1_id]->mapping[button_id].play_color_off );
    if (rec_bank==255)
    {
        if (all_f1[f1_id]->value[PRE_VAL_F1][button_id] == 0)
        {
            all_f1[f1_id]->set_led(button_id, all_f1[f1_id]->mapping[button_id].play_color_off);
        }
        else
        {
            all_f1[f1_id]->set_led(button_id, all_f1[f1_id]->mapping[button_id].play_color_on);
        }
    }
    if (all_f1[f1_id]->fader_on)
    {
        all_f1[f1_id]->set_led(FADER, all_f1[f1_id]->mapping[FADER].play_color_on);
    }
    if (all_f1[f1_id]->knob_on)
    {
        all_f1[f1_id]->set_led(KNOB, all_f1[f1_id]->mapping[KNOB].play_color_on);
    }
}

void update_f1_button_led(uint8_t f1_id, byte data1,byte data2)
{
    if (data2 == 0)
        {
            all_f1[f1_id]->set_led(data1, all_f1[f1_id]->mapping[data1].play_color_off);
        }
        else if (data2 == 127)
        {
            all_f1[f1_id]->set_led(data1, all_f1[f1_id]->mapping[data1].play_color_on);
        }
            all_f1[f1_id]->send_to_f1();
}

void traktor_set_colors()
{
    byte channel = 0;
    byte data1 = 0; // cc
    byte data2 = 0; // value

    if (usbMIDI.read())
    {
        byte cable = usbMIDI.getCable();
   
        if (cable == 0)
        {
            channel = usbMIDI.getChannel();
            data1 = usbMIDI.getData1(); // cc
            data2 = usbMIDI.getData2(); // value
            /*Serial.println("channel");
            Serial.println(channel, DEC);
            Serial.println("data1");
            Serial.println(data1, DEC); // cc
            Serial.println("data2");
            Serial.println(data2, DEC); // value*/

            if (channel == 8)
            {
                update_f1_button_led(0, data1, data2);
            }
            if (channel == 9)
            {
                update_f1_button_led(1, data1, data2);
            }
            if (channel == 10)
            {
                //encoder[data1].writeCounter((int32_t) data2);//faut il le faire?
                //encoder_value[data1]=data2;// risques de boucle??
                receive_encoder_value[data1]= data2;
                RingUpdate(data2, data1);
                midi_reveive_enc_value=true;
                encoder[data1].writeCounter(data2);
            }
            if(channel==11)
            {
                TrellisUpdate(channel,data2,data1);
            }
            if(channel==12)
            {
                TrellisUpdate(channel,data2,data1);
            }
            if(channel==15)
            {
		        // for(int i = 0; i<2; i++)
        	    // {
 			    //     if(all_f1[i]->value[PRE_VAL_F1][CAPTURE]==127)
                //     {
				//     launch_record(i);
			    //     }
		        // }
    		
            }
        }
    }
}

// Set digits
 //
void update_digit(uint8_t f1_id, uint8_t value)
{
    int dizaine = value / 10;
    int unite = value % 10;
    dizaine = all_f1[f1_id]->nb_recorded / 10;
    unite = all_f1[f1_id]->nb_recorded % 10;
    memcpy(all_f1[f1_id]->out_state.right_digit, digit[unite + 1], sizeof(uint8_t) * 8);
    memcpy(all_f1[f1_id]->out_state.left_digit, digit[dizaine + 1], sizeof(uint8_t) * 8);
    if (unite > 0)
    {
        all_f1[f1_id]->set_led(LAUNCH, all_f1[f1_id]->mapping[button_id].play_color_on);
        
    }
        all_f1[f1_id]->send_to_f1();
}

void reset_digit(uint8_t f1_id)
{
    // Serial.printf("reset digit \n" );
    memcpy(all_f1[f1_id]->out_state.right_digit, digit[1], sizeof(uint8_t) * 8);
    memcpy(all_f1[f1_id]->out_state.left_digit, digit[1], sizeof(uint8_t) * 8);
    all_f1[f1_id]->send_to_f1();
}

// Shows list of connected / disconnected devices claimed by a driver
//

void update_device_info()
{
    for (uint8_t i = 0; i < CNT_HIDDEVICES; i++)
    {
        if (*hiddrivers[i] != hid_driver_active[i])
        {
            if (hid_driver_active[i])
            {
                Serial.printf("Device %s disconnected\n", hid_driver_names[i]);
                hid_driver_active[i] = false;
                init_tab[i]=true;
            }
            else
            {
                new_device_detected = true;
                Serial.printf("Device %s connected\n  Vendor ID: %x\n  Device Id : %x \n", hid_driver_names[i], hiddrivers[i]->idVendor(), hiddrivers[i]->idProduct());
                hid_driver_active[i] = true;
            }
        }
    }
}

void set_f1_initial_state(uint8_t f1_id)
{
    DEBUG_PRINT("Start")
    //Serial.printf("initiale state \n f1_id %d \t all f1 f1 id: %d \n", f1_id ,all_f1[f1_id]->f1_id );
    memcpy(all_f1[f1_id]->out_state.right_digit, digit[1], sizeof(uint8_t) * 8);
    memcpy(all_f1[f1_id]->out_state.left_digit, digit[1], sizeof(uint8_t) * 8);
    for (int i = 0; i < 28; i++)
    {
        set_initial_color(i,f1_id);
    }
        all_f1[f1_id]->send_to_f1();
}

void F1_read()
{
    for(int i = 0 ; i<2; i++)
    {
        if (all_f1[i]->has_state_changed())
        {
            button_id = all_f1[i]->get_button_id();
            fader_id = all_f1[i]->get_fader_id();
            knob_id = all_f1[i]->get_knob_id();
            fader_change(fader_id,i);
            knob_change(knob_id,i);//probleme cc255

            if (all_f1[i]->encoder_value != all_f1[i]->in_state.encoder)    
            {
                if(all_f1[i]->get_encoder_direction()==0){
                    midi_send_to_traktor(all_f1[change_f1_id]->channel,40, 0);
                    all_f1[i]->encoder_value = all_f1[i]->in_state.encoder;
                }
                else{
                    midi_send_to_traktor(all_f1[change_f1_id]->channel,40, 127  );
                    all_f1[i]->encoder_value = all_f1[i]->in_state.encoder;
                }
            }
            if(button_id != 255)
            {
                if(rec_bank==UNDEFINE_VAL)
                {
                    if ((button_id > 2 && button_id < 6) || button_id > 7 )//inclus capture à type
                    {
                        button_pressed_MIDI(button_id, i);
                    }
                    else
                    {
                        button_pressed_intern( button_id, i);
                    }
                }
                else
                {
                    if (button_id > 2)
                    {
                        button_pressed_rec( button_id,i, rec_bank);
                    }
                    else
                    {
                        button_pressed_intern( button_id,i);
                    }
                }  
            }
            all_f1[i]->clear_change_flag(); 
        }   
    }
}

void F1_plug()
{
    for (int i = 0; i < 2; i++)
    {
        if (hid_driver_active[i])
        {
            while (init_tab[i]==true)
            {
                ::Serial.printf("initab %d \t f1n°:%d\n  %d",init_tab[i],i);
                if (i==0){
                    all_f1[i]->mapping = mapping;
                }
                else{
                    all_f1[i]->mapping = mapping2;
                }
                
                all_f1[i]->channel = i;
                all_f1[i]->f1_id = 0;
                all_f1[i]->nb_recorded = 0;
                all_f1[1]->fader_on = true;
                all_f1[1]->knob_on = true;
                for (int j = 0; j < 5; j++)
                {
                    //all_f1[i]->value_fader[j] = 0;
                }
                for (int k = 1; k < 3; k++)
                {
                    for (int l = 0; l < TRAKTOR_F1_BUTTON_COUNT; l++)
                    {
                        all_f1[i]->value[PRE_VAL_F1][l] = 0;
                        all_f1[i]->value[REC_PRE_VAL_ENCODERS][l] = 0;
                        all_f1[i]->value[k][l] = 255;// j: 1=REC_PRE_VAL_F1, 2=REC2_PRE_VAL_F1
                    }
                }
                for (int m = 0; m < 2; m++)
                {
                    for (int n = 0; n < 4; n++)
                    {
                        all_f1[i]->store_record[m][n] = 255;
                    }
                }
                set_f1_initial_state(i);
                init_tab[i]=false;
            }
        }
    }
}    
//=============================================================================
// TRELLIS
//=============================================================================                              

TrellisCallback fx_on(keyEvent evt){
   //Check is the pad pressed?
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    //trellis1.pixels.setPixelColor(evt.bit.NUM,ORANGE); //on risin
    midi_send_to_traktor(5,evt.bit.NUM,127);
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    midi_send_to_traktor(5,evt.bit.NUM,0);
 //off falling
  }
  trellis1.pixels.show();
  return 0;
}
TrellisCallback fx_on2(keyEvent evt){
   //Check is the pad pressed?
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    //trellis2.pixels.setPixelColor(evt.bit.NUM,ORANGE); //on risin
    midi_send_to_traktor(6,evt.bit.NUM,127);
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    midi_send_to_traktor(6,evt.bit.NUM,0);
    ; //off falling
  }
  trellis2.pixels.show();
  return 0;
}
void TrellisUpdate(u_int8_t trellis,u_int8_t value, u_int8_t id){
    
    if(trellis==11){
        if(value==127){
            trellis1.pixels.setPixelColor(id,ORANGE);
        }
        else{
            if(value==0){
                if(id<8){
                    trellis1.pixels.setPixelColor(id,BLEU_VIOLET);    
                }
                else{
                    trellis1.pixels.setPixelColor(id,BLEU_PALE);
                }
            }
        }
    }else{
        if(trellis==12){
            if(value==127){
                trellis2.pixels.setPixelColor(id,ORANGE);
            }
            else{
                if(value==0){
                    if(id<8){
                        trellis2.pixels.setPixelColor(id,VERT_CLAIR2);    
                    }
                    else{
                        trellis2.pixels.setPixelColor(id,ROSE_CLAIR);
                    }
                }
            }
        }
    }
}

//=============================================================================
// Setup
//=============================================================================
void setup()
{
    Serial.begin(115200);
    while (!Serial && millis() < 1000); // wait for Serial Monitor for a little while

   
            // Check if any device already connected
    Wire2.begin();
    for (int i = 0; i < NB_ENCODER; i++)
    {
        encoder[i].begin(econfig);
        encoder[i].writeCounter(counter);
        encoder[i].writeMax(maxvalue);
        encoder[i].writeMin(minvalue);
        encoder[i].writeStep(Step);
        encoder[i].writeDoublePushPeriod(45);
        encoder[i].updateStatus();
        encoder[i].onButtonRelease = encoder_push;
        encoder[i].onButtonDoublePush = encoder_double_push;
        encoder[i].id=i;
        encoder_value[i] = 0;
        receive_encoder_value[i]= 0;
        encoder_message[i].channel = 255;
        encoder_message[i].control = 0;
        encoder_message[i].value = 0;
    }
    
    Wire1.begin();
    Wire.begin();
       Serial.printf("rec_cadrant_droit:led\n ");
    for (int i = 0; i < NB_RING; i++)
    {
        encoder_led[i]->LEDRingSmall_Reset();
        delay(20);
        encoder_led[i]->LEDRingSmall_Configuration(0x01); // Normal operation
        encoder_led[i]->LEDRingSmall_PWMFrequencyEnable(1);
        encoder_led[i]->LEDRingSmall_SpreadSpectrum(0b0010110);
        encoder_led[i]->LEDRingSmall_GlobalCurrent(0x10);
        encoder_led[i]->LEDRingSmall_SetScaling(0xFF);
        encoder_led[i]->LEDRingSmall_PWM_MODE();
        encoder_led[i]->LEDRingSmall_Set_RGB(0, 0x0000FF);
    }
    myusb.begin();
    //Treelis1
    trellis1.begin();
    for(int i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
            trellis1.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
            trellis1.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
            trellis1.registerCallback(i, fx_on);
            trellis1.pixels.setBrightness(20);
        }
    trellis1.pixels.setPixelColor(0, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(1, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(2, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(3, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(4, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(5, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(6, BLEU_VIOLET);
    trellis1.pixels.setPixelColor(7, BLEU_VIOLET);

    trellis1.pixels.setPixelColor(8, BLEU_PALE);
    trellis1.pixels.setPixelColor(9, BLEU_PALE);
    trellis1.pixels.setPixelColor(10, BLEU_PALE);
    trellis1.pixels.setPixelColor(11, BLEU_PALE);
    trellis1.pixels.setPixelColor(12, BLEU_PALE);
    trellis1.pixels.setPixelColor(13, BLEU_PALE);
    trellis1.pixels.setPixelColor(14, BLEU_PALE);
    trellis1.pixels.setPixelColor(15, BLEU_PALE);
trellis1.pixels.show();

    trellis2.begin();
    for(int i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
            trellis2.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
            trellis2.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
            trellis2.registerCallback(i, fx_on2);
            trellis2.pixels.setBrightness(20);
        }
    trellis2.pixels.setPixelColor(0, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(1, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(2, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(3, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(4, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(5, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(6, VERT_CLAIR2);
    trellis2.pixels.setPixelColor(7, VERT_CLAIR2);

    trellis2.pixels.setPixelColor(8, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(9, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(12, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(13, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(10, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(11, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(14, ROSE_CLAIR);
    trellis2.pixels.setPixelColor(15, ROSE_CLAIR);
    trellis2.pixels.show();

    Serial.printf("TRE\n ");
   
    

 
}
//=============================================================================
// Loop
//=============================================================================
void loop()
{
   myusb.Task();
   update_device_info();
   F1_plug() ;             
    traktor_set_colors();
    encread();
    F1_read(); 
    trellis1.read();
    trellis2.read();  
  
   
    
   
          
}
//=============================================================================
// OLD
//=============================================================================
