//#define DEBUG
#include <debug.h>

#include <Arduino.h>
#include "TraktorController.h"

// 7 segment display main states
//     A
//    ---
//  F| G |B
//    ---
//  E|   |C
//    ---  . DP
//     D
uint8_t digit[11][8] = {
//   DP    G     C     B     A     F     E     D
    {0   , 0   , 0   , 0   , 0   , 0   , 0   , 0    },
    {0   , 0   , 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f },
    {0   , 0   , 0x7f, 0x7f, 0   , 0   , 0   , 0    },
    {0   , 0x7f, 0   , 0x7f, 0x7f, 0   , 0x7f, 0x7f },
    {0   , 0x7f, 0x7f, 0x7f, 0x7f, 0   , 0   , 0x7f },
    {0   , 0x7f, 0x7f, 0x7f, 0   , 0x7f, 0   , 0    },
    {0   , 0x7f, 0x7f, 0   , 0x7f, 0x7f, 0   , 0x7f },
    {0   , 0x7f, 0x7f, 0   , 0x7f, 0x7f, 0x7f, 0x7f },
    {0   , 0   , 0x7f, 0x7f, 0x7f, 0   , 0   , 0    },
    {0   , 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f },
    {0   , 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0   , 0x7f }
};
extern uint8_t rec_button_id;
extern uint8_t launch_button_id;
extern uint8_t cancel_button_id;
extern uint8_t rec_mode;

#define PRE_VAL_F1 0
#define REC_PRE_VAL_F1 1
#define REC_PRE_VAL_OTHER_F1 2
#define REC_PRE_VAL_ENCODERS 3
//
//
//
void print_hexbytes(const void *ptr, uint32_t len)
{
	if (ptr == NULL || len == 0) return;
	const uint8_t *p = (const uint8_t *)ptr;
	for(uint32_t i=0;i<len;i++) {
		if (*p < 16) Serial.print('0');
		Serial.print(*p++, HEX);
		Serial.print(' ');
		if(i % 8 == 7) Serial.println();
	}
	Serial.println();
}

//
//
//
void TraktorController::init()
{
	USBHost::contribute_Transfers(mytransfers, sizeof(mytransfers)/sizeof(Transfer_t));
	USBHIDParser::driver_ready_for_hid_collection(this);
}

//
//
//
hidclaim_t TraktorController::claim_collection(USBHIDParser *driver, Device_t *dev, uint32_t topusage)
{
	DEBUG_PRINTF("Claim: %x:%x usage: %x\n", dev->idVendor, dev->idProduct, topusage);

	if (dev->idVendor != 0x17cc || dev->idProduct != 0x1120) {
		DEBUG_PRINT("Not claimed - Vendor / Product not matching");
		return CLAIM_NO;
	}
	if (mydevice != NULL && dev != mydevice) {
		DEBUG_PRINT("Not claimed");
		return CLAIM_NO;
	}

	if (usage_) {
		DEBUG_PRINT("Not claimed");
		return CLAIM_NO;			// Only claim one
	}

	if (fixed_usage_ && fixed_usage_ != topusage ) {
		DEBUG_PRINT("Not claimed - fixed_usage_ != topusage");
		return CLAIM_NO; 		// See if we want specific one and if so is it this one
	}

	mydevice = dev;
	collections_claimed++;
	usage_ = topusage;
	driver_ = driver;	// remember the driver.

	DEBUG_PRINT("Claimed !");
	return CLAIM_INTERFACE;  // We wa
}

//
//
//
void TraktorController::disconnect_collection(Device_t *dev)
{
	if (--collections_claimed == 0) {
		mydevice = NULL;
		usage_ = 0;
	}
}


//
// Using hid_process_in_data to parse received data.
// Not using default parser based on HID reports
//
bool TraktorController::hid_process_in_data(const Transfer_t *transfer) {
				#ifdef DEBUG
				Serial.printf("transfer hid_process_in_data: \n");
				print_hexbytes(&transfer->buffer,64);
				Serial.printf("transfer  end hid_process_in_data \n");
				print_hexbytes(raw_state,TRAKTOR_F1_IN_PACKET_SIZE);
				#endif
	// Storing data as received and parsing
	// if usage matches
	if (usage_ == 0xff010000) {

		// Hmmmm... packet is 64 bytes while only 22 are used... We'll copy only the 22 bytes required.

		if(transfer->length != TRAKTOR_F1_IN_MAX_PACKET_SIZE) {
			Serial.printf("ERR Houston, we've got a problem... packet too big : %u vs %u !\n",transfer->length,TRAKTOR_F1_IN_MAX_PACKET_SIZE);
		}else{

			if(!state_changed) state_changed = memcmp(transfer->buffer,raw_state,sizeof(uint8_t)*TRAKTOR_F1_IN_PACKET_SIZE);//???
			else Serial.println("WAR Ooops... state_changed not reset since last data... we missed something...");

			// if just the very same data, no use to update,
			// so just doing so if there is effect    ively a change
			if(state_changed){

				memcpy(raw_state,transfer->buffer,sizeof(uint8_t)*TRAKTOR_F1_IN_PACKET_SIZE);

				//storing edges
				uint32_t buttons_changes;
				uint32_t buffer_buttons;
				buffer_buttons=(raw_state[1] << 24) | (raw_state[2] << 16) | (raw_state[4] << 8) | raw_state[3];
				buttons_changes = in_state.button^buffer_buttons;



				#ifdef DEBUG
				Serial.printf("buffer hid in data: \n");
				print_hexbytes(transfer->buffer,transfer->length);
				Serial.printf("raw state hid in data: \n");
				print_hexbytes(raw_state,TRAKTOR_F1_IN_PACKET_SIZE);
				#endif

				// storing state in a more readable struct
				in_state.reportID = raw_state[0];
				in_state.button   = buffer_buttons;
				in_state.encoder  = raw_state[5];
				in_state.knob[0]  = (raw_state[6] << 8) | raw_state[7];
				in_state.knob[1]  = (raw_state[8] << 8) | raw_state[9];
				in_state.knob[2]  = (raw_state[10] << 8) | raw_state[11];
				in_state.knob[3]  = (raw_state[12] << 8) | raw_state[13];
				in_state.fader[0] = (raw_state[14] << 8) | raw_state[15];
				in_state.fader[1] = (raw_state[16] << 8) | raw_state[17];
				in_state.fader[2] = (raw_state[18] << 8) | raw_state[19];
				in_state.fader[3] = (raw_state[20] << 8) | raw_state[21];

/* 				for(int i=0; i<32; i++){
				Serial.printf("in state button %d, %d \n",bitRead(in_state.button, i),i);
				}
				for(int i=0; i<32; i++){
				Serial.printf("buffer button %d, %d \n",bitRead(buffer_buttons, i),i);
				}
				for(int i=0; i<32; i++){
				Serial.printf(" button change %d ,%d \n",bitRead(buttons_changes, i),i);
				} */
				for(int i=0; i<32; i++){
					if(bitRead(buttons_changes, i)==1){
						if(bitRead(in_state.button, i)==1){
							bitWrite(in_state.rising_edge,i,1);
							bitWrite(in_state.falling_edge,i,0);
						}else{
							bitWrite(in_state.rising_edge,i,0);
							bitWrite(in_state.falling_edge,i,1);
						}
					}else{
							bitWrite(in_state.rising_edge,i,0);
							bitWrite(in_state.falling_edge,i,0);
					}
					bitWrite(buttons_changes, i,0);
 					/* Serial.printf("in_state.rising_edge %d, %d \n",bitRead(in_state.rising_edge, i),i);
					Serial.printf("in_state.falling_edge %d, %d \n",bitRead(in_state.falling_edge, i),i);
					Serial.printf("buttons_changes %d, %d \n",bitRead(buttons_changes, i),i);  */
				}
			}
		}
	}else{
		Serial.printf("ERR Unknown usage : %x", usage_);
	}
	return true;
}

bool TraktorController::hid_process_out_data(const Transfer_t *transfer)
{
	//DEBUG_PRINTF("usage:%x", usage_);
	//print_hexbytes(transfer->buffer,transfer->length);
	return true;
}

void TraktorController::send_to_f1() {
	DEBUG_PRINT("Start\n");
	//Serial.printf("send to f1 \n");
	#ifdef DEBUG
	DEBUG_PRINT("--- out state send to f1\n");
	print_hexbytes(&out_state,TRAKTOR_F1_OUT_PACKET_SIZE);
	DEBUG_PRINT("--- end outstate send to f1\n");
	#endif

	uint8_t buffer[TRAKTOR_F1_OUT_PACKET_SIZE];

	#define TRAKTOR_F1_START_DIGIT   1
	#define TRAKTOR_F1_START_BUTTON 17
	#define TRAKTOR_F1_START_MATRIX 25
	#define TRAKTOR_F1_START_STOP   73

	buffer[0] = out_state.report_id;

	for(int i=0;i<8;i++) {
		buffer[TRAKTOR_F1_START_DIGIT+i] = out_state.right_digit[i];
		buffer[TRAKTOR_F1_START_DIGIT+8+i] = out_state.left_digit[i];
	}

	buffer[TRAKTOR_F1_START_BUTTON]   = out_state.browse;
	buffer[TRAKTOR_F1_START_BUTTON+1] = out_state.size;
	buffer[TRAKTOR_F1_START_BUTTON+2] = out_state.type;
	buffer[TRAKTOR_F1_START_BUTTON+3] = out_state.reverse;
	buffer[TRAKTOR_F1_START_BUTTON+4] = out_state.shift;
	buffer[TRAKTOR_F1_START_BUTTON+5] = out_state.capture;
	buffer[TRAKTOR_F1_START_BUTTON+6] = out_state.quant;
	buffer[TRAKTOR_F1_START_BUTTON+7] = out_state.sync;

	for(int i=0;i<16*3;i++) {
		buffer[TRAKTOR_F1_START_MATRIX+i] = out_state.pad[i];
	}

	for(int i=0;i<4*2;i++) {
		buffer[TRAKTOR_F1_START_STOP+i] = out_state.stop[i];
	}

	#ifdef DEBUG
	DEBUG_PRINT("--- buffer send to F1\n");
	print_hexbytes(buffer,TRAKTOR_F1_OUT_PACKET_SIZE);
	DEBUG_PRINT("--- buffer send to F1\n");
	#endif

	sendPacket(buffer,TRAKTOR_F1_OUT_PACKET_SIZE);
}

bool TraktorController::sendPacket(const uint8_t *buffer,uint32_t length)
{
	DEBUG_PRINT("Start\n");
	#ifdef DEBUG
	//DEBUG_PRINT("--- buffer sendpacket\n");
	print_hexbytes(buffer,TRAKTOR_F1_OUT_PACKET_SIZE);
	DEBUG_PRINT("--- end buffer sendpacket\n");
	#endif
	if (!driver_) return false;
	return driver_->sendPacket(buffer,length);//???
}

void TraktorController::hid_input_begin(uint32_t topusage, uint32_t type, int lgmin, int lgmax)
{
	DEBUG_PRINTF("top usage:%x type:%x lgmin:%x lgmax:%x", topusage, type, lgmin, lgmax);
	// hid_input_begin_ = true;
}

void TraktorController::hid_input_data(uint32_t usage, int32_t value)
{
	DEBUG_PRINTF("usage=%x, index=%x, value=%x", usage, usage & 0xffff, value);
	//if ((value >= ' ') && (value <='~')) DEBUG_PRINTF("(%c)", value);

}

void TraktorController::hid_input_end()
{
	DEBUG_PRINT("hid_input_end");

}

bool TraktorController::is_button_pressed(uint8_t button_id)
{
	return (in_state.button >> button_id) & 0x01;
}

uint16_t TraktorController::get_knob_value(uint8_t knob_id)
{
	return in_state.knob[knob_id];
}

uint16_t TraktorController::get_fader_value(uint8_t fader_id)
{
	return in_state.fader[fader_id];
}

uint8_t TraktorController::get_encoder_value()
{
	Serial.printf("val %d \n",in_state.encoder);

	return in_state.encoder;
}

uint8_t TraktorController::get_encoder_direction()
{
	if(in_state.encoder != encoder_value)
	{
		if((in_state.encoder > encoder_value) && (in_state.encoder != 255))
		{	
			encoder_value = in_state.encoder;
			return 0;
		}
		else{
			encoder_value = in_state.encoder;
			return 127;
		};
	}

	return encoder_value;
}

uint8_t TraktorController::get_fader_id()
{
	uint8_t id=0;
	uint16_t new_value;

	for (int i=0; i<4; i++){
			id=i+30;
			new_value = ((in_state.fader[i]<<8) + (in_state.fader[i]>>8))/32;
			DEBUG_PRINTF("fader_id: %d %d",i,id);
			if(value_fader[i] != new_value) {
				value_fader[i] = new_value;
				return id;
			}
	}
	return 255;
} 
uint8_t TraktorController::get_knob_id()
{
	uint8_t id=0;
	uint16_t val=0;
	uint16_t resol;

	for (int i=0; i<4; i++){
			id=i+34;
			resol=in_state.knob[i]<<8;
			val=in_state.knob[i]>>8;
			DEBUG_PRINTF("id: %d %d",i,id);
			if(value_knob[i]!=(resol+val)/32){
				value_knob[i]=(resol+val)/32;
				Serial.printf("ID %d \n",id);
				return id;
			}
	}
	return 255;
} 

void TraktorController::set_led(uint8_t button_id, uint8_t color[3])
{	
	int i = 0;
	i=(button_id-7)*3;
	int j = 0;
	j=(button_id-23)*2;

	switch(button_id){
		case 0 :
			out_state.sync = color[0];
		break;
		case 1 :
			out_state.quant = color[0];
		break;
		case 2 :
			out_state.capture = color[0];
		break;
		case 3 :
			out_state.shift = color[0];
		break;
		case 4 :
			out_state.reverse = color[0];
		break;
		case 5 :
			out_state.type = color[0];
		break;
		case 6 :
			out_state.size = color[0];
		break;
		case 7 :
			out_state.browse = color[0];
		break;
		default:
			if(button_id>23){
				out_state.stop[j-2]=color[0];
				out_state.stop[j-1]=color[1];

			}else{
				out_state.pad[i-3]=color[0];
				out_state.pad[i-2]=color[1];
				out_state.pad[i-1]=color[2];
			}
	}
}


bool TraktorController::is_rising_egde(uint8_t  button_id)
{	
	if(bitRead(in_state.rising_edge,get_bit_position_with_id [button_id])==1){
		return true;
	}
	return false;
}

bool TraktorController::is_falling_egde(uint8_t  button_id)
{
	if(bitRead(in_state.falling_edge,get_bit_position_with_id [button_id])==1){
		return true;
	}
	return false;
}

uint8_t TraktorController::get_button_id(){	
	for(int i=0; i<32;i++){
		if((bitRead(in_state.rising_edge,i))==1)
		{
			button_id=get_id_with_bit_position[i];
			return button_id;
		}
		else if ((bitRead(in_state.falling_edge,i))==1)
		{
			if (button_id>7 && button_id<28)
			{
				button_id=get_id_with_bit_position[i];
				return button_id;
			}
		}

	}
	return 255;
}
uint8_t TraktorController::id_traktor_has_changes(){	
	if (state_changed){
	return f1_id;
	}return 255;
}
//================================================================
// Unused
//================================================================
//void TraktorController::set_digits(uint8_t value) {
//}

//void TraktorController::set_digits(uint8_t value) {
//}

/*uint8_t TraktorController::boutton_toggle(uint8_t  button_id)
{
	uint8_t  val=0;

	if((bitRead(in_state.rising_edge,get_bit_position_with_id [button_id]))==0){
		bitWrite(in_state.rising_edge,get_bit_position_with_id [button_id],1);
		if(rec_mode && button_id != rec_button_id && button_id != launch_button_id && button_id != cancel_button_id ){
			val = value[REC_PRE_VAL_F1][button_id];
		}else{
			val = value[PRE_VAL_F1][button_id];
		}
	    if(val == mapping[button_id].off.value){
			val = mapping[button_id].on.value;
		}else{
			if (button_id>7 && button_id < 24)
			{
				val = mapping[button_id].on.value;
			}
			else
			{
				val = mapping[button_id].off.value;
			}
		}
		if(rec_mode && button_id != rec_button_id && button_id != launch_button_id && button_id != cancel_button_id){
			value[REC_PRE_VAL_F1][button_id] = val;
		}else{
			   ;
		}
	}return val;
		
}*/