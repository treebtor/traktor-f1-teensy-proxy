#ifndef TRAKTORCONTROLLER_
#define TRAKTORCONTROLLER_

#include <USBHost_t36.h>
#include <MIDIUSB.h>

#define TRAKTOR_VENDOR_ID  0x17cc
#define TRAKTOR_F1_PRODUCT_ID 0x1120

#define TRAKTOR_F1_IN_PACKET_SIZE 22
#define TRAKTOR_F1_IN_MAX_PACKET_SIZE 64
#define TRAKTOR_F1_OUT_PACKET_SIZE 81

#define TRAKTOR_F1_BUTTON_COUNT 28

extern uint8_t digit[11][8];

// Position of buttons LED states in MIDI to F1 mapping
#define BUTTON_LED_SYNC       0
#define BUTTON_LED_QUANT      1
#define BUTTON_LED_CAPTURE    2
#define BUTTON_LED_SHIFT      3
#define BUTTON_LED_REVERSE    4
#define BUTTON_LED_TYPE       5
#define BUTTON_LED_SIZE       6
#define BUTTON_LED_BROWSE     7

#define BUTTON_LED_MATRIX_01  8
#define BUTTON_LED_MATRIX_02  9
#define BUTTON_LED_MATRIX_03 10
#define BUTTON_LED_MATRIX_04 11
#define BUTTON_LED_MATRIX_05 12
#define BUTTON_LED_MATRIX_06 13
#define BUTTON_LED_MATRIX_07 14
#define BUTTON_LED_MATRIX_08 15
#define BUTTON_LED_MATRIX_09 16
#define BUTTON_LED_MATRIX_10 17
#define BUTTON_LED_MATRIX_11 18
#define BUTTON_LED_MATRIX_12 19
#define BUTTON_LED_MATRIX_13 20
#define BUTTON_LED_MATRIX_14 21
#define BUTTON_LED_MATRIX_15 22
#define BUTTON_LED_MATRIX_16 23

#define BUTTON_LED_STOP_1    24
#define BUTTON_LED_STOP_2    25
#define BUTTON_LED_STOP_3    26
#define BUTTON_LED_STOP_4    27

#define MIDI_CC_ON 127
#define MIDI_CC_OFF 0

//
// Data structure for HID Data OUT
//
typedef struct {
    uint8_t report_id = 0x80;

    uint8_t right_digit[8];		// Each byte in the array corresponds to segment: DP; G; C; B; A; F; E; D
    uint8_t left_digit[8];		// Value = brightness

    uint8_t browse;             // Illuminated button state : BUTTON_LED_ON or BUTTON_LED_OFF
    uint8_t size;
    uint8_t type;
    uint8_t reverse;
    uint8_t shift;
    uint8_t capture;
    uint8_t quant;
    uint8_t sync;

    uint8_t pad[16*3];			// Each byte in the array corresponds to color: Blue; Red; Green

    uint8_t stop[4*2];			// Each byte in array corresponds to brightness of one of two LEDS

} F1OutputData_t;

//
// Data structure for HID Data IN
//
typedef struct {

    uint8_t  reportID;			// 0x01
    uint32_t button;	        // States of all buttons
    uint8_t  encoder;		    // Value of encoder
    uint16_t fader[4];		    // Values of the 4 faders
    uint16_t knob[4];	        // Values of the 4 knobs
    uint32_t rising_edge;	// edges for buttons
    uint32_t falling_edge;	//

} F1InputData_t;

//
// Control change message
//
typedef struct {
    uint8_t channel;
    uint8_t control;
    uint8_t value;
} control_change;


//
// Data structure for mapping
//
typedef struct {
    uint8_t play_color_off[3];
    uint8_t play_color_on[3];
    uint8_t record_color_off[3];
    uint8_t record_color_on[3];
    control_change off;
    control_change on;
} midi_to_f1_mapping;

// Taktor F1 Controller (for now, may be extended to some others)
//
class TraktorController : public USBHIDInput {
public:
    F1InputData_t in_state;                 // Traktor current state (from last IN data received)
                                            // Should it be private with getters/setters methods ?
    F1OutputData_t out_state;               //Traktor current state (from last OUT data send)


    //uint8_t modified_button_list[TRAKTOR_F1_BUTTON_COUNT];
    //uint8_t modified_buttons_count;
    bool  rec_mode=false;
    bool  fader_on=false;
    bool  knob_on=false;
    uint8_t f1_id;
    uint8_t button_id;
    uint8_t channel;     
    uint8_t value[4][TRAKTOR_F1_BUTTON_COUNT];
    uint8_t store_record[2][4];    
    uint8_t get_id_with_bit_position [32]= {255 ,255 ,29,7,6,5,4,3,255,2,1 ,0 ,24,25,26,27,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8}; // inverser les 4 dernier    
    uint8_t get_bit_position_with_id [32]= {11,10,9,7,6,5,4,3,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,12,13,14,15,255,255,255,255};
    uint16_t value_fader[4];
    uint16_t value_knob[4];
    uint8_t encoder_value=0;
    uint8_t modified_fader_count;

    uint8_t modified_knob_list[4];
    uint8_t modified_knob_count;
    uint8_t nb_recorded;
    

    midi_to_f1_mapping *mapping;

    TraktorController(USBHost &host, uint32_t usage = 0) : fixed_usage_(usage) { init(); }
    uint32_t usage(void) {return usage_;}
    //void attachReceive(bool (*f)(uint32_t usage, const uint8_t *data, uint32_t len)) {receiveCB = f;}
    bool sendPacket(const uint8_t *buffer, uint32_t len);

    // Getters
    bool is_button_pressed(uint8_t button_id);
    //uint8_t boutton_toggle(uint8_t  button_id);         //return new value of control change compared with previous value
    bool is_rising_egde(uint8_t  button_id);
    bool is_falling_egde(uint8_t  button_id);
    
    uint8_t get_button_id(); 
    uint16_t get_knob_value(uint8_t knob_id);
    uint16_t get_fader_value(uint8_t fader_id);
    uint8_t get_fader_id();
    uint8_t get_knob_id();
    uint8_t get_encoder_value();
    uint8_t get_encoder_direction();

    uint8_t id_traktor_has_changes();

    uint8_t get_modified_buttons(uint8_t *button_list);
    uint8_t get_modified_faders(uint8_t *fader_list);
    uint8_t get_modified_knobs(uint8_t *knob_list);

    uint8_t id_button_from_cc_mapping(uint8_t cc);

    void set_led(uint8_t button, uint8_t color[3]);
    void set_digits(uint8_t value);


    void send_to_f1(); // Send new state to F1

    void clear_change_flag()  { state_changed = false; } // Once change received, user should clear the change flag
    bool has_state_changed() { return state_changed; } // Has state changed since last time user had a look


protected:
    virtual hidclaim_t claim_collection(USBHIDParser *driver, Device_t *dev, uint32_t topusage);
    virtual bool hid_process_in_data(const Transfer_t *transfer);
    virtual bool hid_process_out_data(const Transfer_t *transfer);
    virtual void hid_input_begin(uint32_t topusage, uint32_t type, int lgmin, int lgmax);
    virtual void hid_input_data(uint32_t usage, int32_t value);
    virtual void hid_input_end();
    virtual void disconnect_collection(Device_t *dev);
private:
    void init();
//    bool (*receiveCB)(uint32_t usage, const uint8_t *data, uint32_t len) = nullptr;

    USBHIDParser *driver_;

    bool state_changed = false;
    bool data_received = false;

    uint8_t raw_state[TRAKTOR_F1_IN_PACKET_SIZE];  // Raw state (from IN HID data)

    enum { MAX_PACKET_SIZE = 64 };
    uint8_t collections_claimed = 0;
    volatile bool hid_input_begin_ = false;
    uint32_t fixed_usage_;
    uint32_t usage_ = 0;

    // See if we can contribute transfers
    Transfer_t mytransfers[2] __attribute__ ((aligned(32)));

};

#endif
